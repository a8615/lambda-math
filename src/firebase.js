// Import the functions you need from the SDKs you need

import { initializeApp } from "firebase/app";

import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword } from "firebase/auth"

import { getFirestore } from "firebase/firestore";



// Your web app's Firebase configuration

const firebaseConfig = {

  apiKey: "AIzaSyDlcGLYWdkPdK8HZUQcAmWdabiw1e8mKxo",

  authDomain: "lambda-math.firebaseapp.com",

  projectId: "lambda-math",

  storageBucket: "lambda-math.appspot.com",

  messagingSenderId: "303551453156",

  appId: "1:303551453156:web:f8cad2abd9973939ba19f1"

}


// Initialize Firebase

const app = initializeApp(firebaseConfig)

export const auth = getAuth(app)

export const db = getFirestore(app)

export const sign_in_with = signInWithEmailAndPassword

export const sign_up_with = createUserWithEmailAndPassword