# Lambda Math

Lambda Math is a web page that asks you [lambda calculus](https://en.wikipedia.org/wiki/Lambda_calculus) related questions.

## Usage

You are able to navigate to a place that tests you on certain lambda calculus concepts. Go to the website: [Lambda Math](https://lambdamath.neocities.org)

## Built With

* [svelte](https://svelte.dev/) - JS framework/"front end compiler"
* [vite](https://vitejs.dev/) - Developing tool
* [vscodium](https://vscodium.com/) - Fantastic text editor

## Authors

* **Geographicallylazy** (https://gitlab.com/geographicallylazy)

## License

For now, just give me credit and share your changes with others, please.
